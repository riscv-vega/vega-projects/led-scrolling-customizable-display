# LED SCROLLING DISPLAY BOARD WITH CUSTOMIZABLE MESSAGE

## Introduction
In this tutorial we will learn how to use 8x8 LED Matrix to display customize text message with VEGA ARIES Boards. 
Here we will use Bluetooth Application to customize text message and SD Card Module to read existing text file.

<img src="images/example_1.jpg" alt="image" width="1000"/>

## Overview
For this project I am using ARIES v2 board, but you can use any [VEGA ARIES board](https://vegaprocessors.in/devboards/). We will make two examples. In [Example 01](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display#example-01) we will simply customize scrolling text on 8x8 LED Matrix via Bluetooth and custom build Android application. And in [Example 02](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display#example-02) we will read text file from SD Card and display it on 8x8 LED Matrix.

## Prerequisites
- Windows 10 or above/Linux (64 bit)
- Arduino IDE 
- [VEGA ARIES Board support package](https://vegaprocessors.in/devboards/ariesv2.html)

## Hardware Required
- VEGA ARIES Board
- MAX7219 Dot Led Matrix Module 
- Bluetooth Module HC-05
- Micro SD Card Reader Module (Optional - for Example 02)
- Micro USB type B to USB type A cable.
- Jumper wires

#### VEGA ARIES Boards
The ARIES v2.0 is a fully indigenous and a “Made in India” product to get started with basic microprocessor programming and embedded systems. This board is built upon a [RISC-V ISA](https://riscv.org/technical/specifications/) compliant [VEGA Processor](https://vegaprocessors.in/) with easy-to-use hardware and software. The VEGA SDK also provides full ecosystem with numerous examples and support [documentation](https://vegaprocessors.in/tuts.html).

<img src="aries_v2_pinout.jpeg" alt="aries_v2_pinout" width="400"/>

#### MAX7219 Dot Led Matrix Module
The MAX7219/MAX7221 are compact, serial input/output common-cathode display drivers that interface microprocessors (µPs) to 7-segment numeric LED displays of up to 8 digits, bar-graph displays, or 64 individual LEDs.

<img src="https://lastminuteengineers.b-cdn.net/wp-content/uploads/arduino/MAX7219-Module-Variants.jpg" alt="MAX7219" width="600"/>

#### Bluetooth Module HC-05
The HC-05 is a class 2 Bluetooth module designed for transparent wireless serial communication. It is pre-configured as a slave Bluetooth device. Once it is paired to a master Bluetooth device such as PC, smart phones and tablet, its operation becomes transparent to the user. All data received through the serial input is immediately transmitted over the air. When the module receives wireless data, it is sent out through the serial interface exactly at it is received. 

<img src="https://5.imimg.com/data5/SELLER/Default/2020/8/OK/JZ/CA/3012225/hc-05-6pin-bluetooth-module-master-slave-with-button-500x500.png" alt="hc05" width="100"/>

#### Micro SD Card Reader Module
Micro SD Card Reader Module also called Micro SD Adaptor which is designed for dual I/O voltages. The Module is a simple solution for transferring data to and from a standard SD card. The pinout is directly compatible with any microcontrollers which has SPI interface.

<img src="https://m.media-amazon.com/images/I/61RLAs0kZxL.jpg" alt="sd_card_module" width="100"/>

## Libraries Required
Download the MD_MAX72XX Library from `Tools >> Manage Libraries..`

<img src="screenshots/librariesRequired.png" alt="Download_Library" width="1000"/>

## Let's get started
Power up the ARIES v2 board via the USB port of a Laptop/PC, for that we have to use a micro USB type B to USB type A cable. The cable should be connected to UART-0 port of the ARIES v2 board. 

### [Example 01](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display/-/tree/master/ledDisplayWithCustomisableMessage?ref_type=heads)

Now let’s connect the 8×8 LED Matrix module and HC-05 Bluetooth module to the ARIES v2 board.

#### Connection Diagram:
<img src="CustomizableLEDDisplay.png" alt="Connection_diagram_Ex1" width="1000"/>


> [!NOTE] 
> You can use any Ground (GND) and 3.3V Supply from the board.


#### Arduino Code:
Once we are done with the connections we are ready to take a look at the Arduino code of the first example.
```javascript
/* 
    Controlling 8×8 LED Matrix via Bluetooth Example 01
    by VEGA Processors, www.vegaprocessors.in

    Library Required: MD_MAX72xx [https://github.com/MajicDesigns/MD_MAX72XX]
*/

#include <SPI.h>
#include <MD_MAX72xx.h>
#include <esp8266.h>

SPIClass SPI(1);
ESP8266Class Bluetooth(1);

// Define the number of devices we have in the chain and the hardware interface
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4 
#define CS_PIN    10  // connect CS pin to GPIO-10
/* NOTE: You can use any GPIO Pin from the board */

MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

char stringBuffer[1000] = {0, };
int count;

#define  DELAYTIME  100  // in milliseconds

void scrollText(const char *p)
{
  uint8_t charWidth;
  uint8_t cBuf[8];  // this should be ok for all built-in fonts

  mx.clear();

  while (*p != '\0')
  {
    charWidth = mx.getChar(*p++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);

    for (uint8_t i=0; i<=charWidth; i++)  // allow space between characters
    {
      mx.transform(MD_MAX72XX::TSL);
      if (i < charWidth)
        mx.setColumn(0, cBuf[i]);
      delay(DELAYTIME);
    }
  }
}

void setup() {
  // Open serial communications and wait for port to open:
  delay(1000);
  Serial.begin(115200);
  Bluetooth.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  mx.begin();
}

void loop() {
  count = 0;
  for(;count<1000;count++){
    stringBuffer[count] = 0;
  }
  Serial.print("Ready to receive text....");
  while(1){
    count = 0;
    while(Bluetooth.available() > 0){
      stringBuffer[count++] = Bluetooth.read();
    }
    if(stringBuffer[count-1] == '\n'){
      stringBuffer[count-1] = '\0';
      break;
    }
  }
  Serial.println("received.");
  count = 0;
  for(;count<1000;count++){
    Serial.print(stringBuffer[count]);
  }
  Serial.println();

  while(1){
    scrollText(stringBuffer);
    if(Bluetooth.available() > 0){
      uint8_t newText = Bluetooth.read();
      if(newText == 'N')break;
    }
  }
}
```


#### Description:
First we need to include the **MD_MAX72xx.h** and **SPI.h** libraries, then create the SPI Class object `SPIClass SPI(1)` where parameter **1** indicates the SPI Peripheral we are using as ARIES Boards has 3 SPIs. Add `ESP8266Class Bluetooth(1)` which denotes the UART-1 Peripheral. Set how many modules we use and define the CS Pin.

```javascript
void setup() {
  // Open serial communications and wait for port to open:
  delay(1000);
  Serial.begin(115200);
  Bluetooth.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  mx.begin();
}
```

In the **setup** function we need to initialize the serial communication (UART-0) with 115200 baudrate, and the Bluetooth at its default baud rate of 9600 bits per second. Then we are initializing MAX7219 Dot Led Matrix Module.

```javascript
Serial.print("Ready to receive text....");
while(1){
count = 0;
while(Bluetooth.available() > 0){
    stringBuffer[count++] = Bluetooth.read();
}
if(stringBuffer[count-1] == '\n'){
    stringBuffer[count-1] = '\0';
    break;
}
}
Serial.println("received.");
```
Next, in the **loop** section using `Bluetooth.available()` function we check for incoming serial data from the UART-1 and if that's true using the `Bluetooth.read()` we strat reading the serial port, one byte each iteration and store it into the buffer. This will continue in a loop till it get newline character. Once it get newline character program execution will come out of the `while(1)` loop. 

```javascript
  count = 0;
  for(;count<1000;count++){
    Serial.print(stringBuffer[count]);
  }
  Serial.println();
```
this part of code simply prints the buffer data (on serial monitor), and that’s the messaged typed in the text box of the Android app.

```javascript
  while(1){
    scrollText(stringBuffer);
    if(Bluetooth.available() > 0){
      uint8_t newText = Bluetooth.read();
      if(newText == 'N')break;
    }
  }
```
Here text message stored in buffer will display on LED Matrix until it get 'N' character which means we want the change the text message.

#### Procedure:
Set up the Arduino software as described in [Getting Started with ARIES v2](https://vegaprocessors.in/devboards/ariesv2.html).

- Create a new sketch and copy-paste the [first Code](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display/-/blob/master/README.md?ref_type=heads#arduino-code) in it. 
- Make sure you have select **ARIES v2** board in Tools menu. Select appropriate port and Programmer as **VEGA XMODEM**.
<img src="screenshots/toolsSetting.png" alt="Tools Menu" width="600"/>

- Upload the code in the board and open Serial monitor with 115200 baudrate.
- On your Android phone, turn on the bluetooth and search and tap on `HC-05` in Available devices. Enter pin as "1234" and select OK. Now you can see HC-05 under Paired devices.

<img src="images/bluetoothPairing.png" alt="phoneBluetooth" width="600"/>

- Now open [Custom Diplay](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display/-/raw/master/Custom_Display.apk?ref_type=heads) App in your phone. Click on **BLUETOOTH** button, select HC-05 and wait till it get connected.
- Enter any text in text box and press on **SEND** button.

<img src="images/bluetoothApp.png" alt="bluetoothApp" width="600"/>

- Finally you can see the text you have send from the Android app scrolling on your LED Matrix. To change the text message, click on **NEW** button, re-enter the text in text box and press **SEND**.

#### Output:
When the code is uploaded `Ready to receive text....` will display on serial monitor. Then we need to send a text message from bluetooth app. Once we did that, the same text message will display on serial monitor.  
<img src="screenshots/Output1.png" alt="output1" width="1000"/>



### [Example 02](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display/-/tree/master/readingFromSDCardandDisplayOnLEDMatrix?ref_type=heads)
Here we will replace bluetooth module with Micro SD Card module.  

#### Connection Diagram:
<img src="readingFromSDCardAndDisplayOnLEDMatrix.png" alt="Connection_diagram_Ex1" width="1000"/>

#### Arduino Code:
Once we are done with the connections, let see how Example 02 works.
```javascript
/* 
    Display text on 8×8 LED Matrix from text file in SD Card Example 02
    by VEGA Processors, www.vegaprocessors.in

    Library Required: MD_MAX72xx [https://github.com/MajicDesigns/MD_MAX72XX]
*/


#include <SPI.h>
#include <SD.h>
#include <MD_MAX72xx.h>

SPIClass SPI(0);
SPIClass SPI_1(1);

// Define the number of devices we have in the chain and the hardware interface
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4
#define CS_PIN_1    10  // connect CS pin of MAX7219 LED Matrix to GPIO-10
#define CS_PIN_2    9   // connect CS pin of SD-CARD Module to GPIO-9

File myFile;
MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, SPI_1, CS_PIN_1, MAX_DEVICES);

char Buffer[1000];
int count;

#define  DELAYTIME  100  // in milliseconds

void setup() {
  // Open serial communications and wait for port to open:
  delay(1000);
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.print("Initializing SD card...");

  if (!SD.begin(CS_PIN_2)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  
  // Initializing LED Matrix
  mx.begin();
//  ** Use below function to remove existing test.txt file from your microSD card **  //
//  removeFile(); 

// ** Use below function to create test.txt file in your microSD card ** //
//  createFile();
}

void loop() {
  count = 0;
  for(;count<1000;count++){
    Buffer[count] = 0;
  }

  if (SD.exists("test.txt")) {
    Serial.println("test.txt exists.");
  } else {
    Serial.println("test.txt doesn't exist.");
    while(1){
      scrollText("test.txt doesn't exist.");
    }
  }
  
  // re-open the file for reading:
  myFile = SD.open("test.txt"); // If you have existing txt file with different name, then change the file name here.
  if (myFile) {
    Serial.println("test.txt:");

    // read from the file until there's nothing else in it:
    count = 0;
    for(;myFile.available();count++){
      Buffer[count] = myFile.read();
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  count = 0;
  for(;count<1000;count++){
    Serial.print(Buffer[count]);
  }
  Serial.println();
  
  while(1){
    scrollText(Buffer);
  }
}

void createFile(void) {  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
    myFile.print("VEGA PROCESSORS"); // ********************************* Modify the text message here **************************** //
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}

void removeFile(void) {
  // use this function to remove text.txt to avoid appending
  if (SD.exists("test.txt")) {
    Serial.print("Removing test.txt...");
    SD.remove("test.txt");
    Serial.println("done.");
  } else {
    Serial.println("test.txt doesn't exist.");
  }
}

void scrollText(const char *p) {
  uint8_t charWidth;
  uint8_t cBuf[8];  // this should be ok for all built-in fonts

  mx.clear();
  while (*p != '\0') {
    charWidth = mx.getChar(*p++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);

    for (uint8_t i=0; i<=charWidth; i++)  // allow space between characters
    {
      mx.transform(MD_MAX72XX::TSL);
      if (i < charWidth)
        mx.setColumn(0, cBuf[i]);
      delay(DELAYTIME);
    }
  }
}

```

#### Description:
In [Example 02](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display#example-02), we are reading `test.txt` file from Micro SD card module and display it on LED Matrix.
```javascript
void setup() {
  // Open serial communications and wait for port to open:
  delay(1000);
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.print("Initializing SD card...");

  if (!SD.begin(CS_PIN_2)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  
  // Initializing LED Matrix
  mx.begin();
}
```

Inside **setup** function we are simply initializing SD card and LED matrix.

```javascript
 if (SD.exists("test.txt")) {
    Serial.println("test.txt exists.");
  } else {
    Serial.println("test.txt doesn't exist.");
    while(1){
      scrollText("test.txt doesn't exist.");
    }
  }
```

Here `SD.exists("file_name")` function will search for the given file name in SD card, if that file exists it will display `test.txt exists.` on Serial monitor. Otherwise it will display `test.txt doesn't exist` on serial monitor as well as on LED Matrix continuously.

```javascript
myFile = SD.open("test.txt");
  if (myFile) {
    Serial.println("test.txt:");

    // read from the file until there's nothing else in it:
    count = 0;
    for(;myFile.available();count++){
      Buffer[count] = myFile.read();
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
```

In above section, if `test.txt` file exists on SD card, the `SD.open("file_name")` function will open the file and read it using `myFile.read()` function. The content of the file will be stored inside the Buffer.

```javascript
  while(1){
    scrollText(Buffer);
  }
```
Finally the text data in the Buffer will display continuously on LED Matrix.

#### Procedure:
Create a new sketch and paste the [second code](https://gitlab.com/riscv-vega/vega-projects/led-scrolling-customizable-display#arduino-code-1) in it. 

- Make sure your micro SD card has `test.txt` file, if not create a new one manually or simply use `createFile()` function call in setup().
- Use `removeFile()` function call to remove existing `test.txt` file.
- Now uplaod the code. Finally you can see the text store in test.txt file scrolling on LED Matrix.  
