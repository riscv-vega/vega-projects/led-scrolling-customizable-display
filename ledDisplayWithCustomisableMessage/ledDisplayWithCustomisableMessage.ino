/* 
    Controlling 8×8 LED Matrix via Bluetooth Example 01
    by VEGA Processors, www.vegaprocessors.in

    Library Required: MD_MAX72xx [https://github.com/MajicDesigns/MD_MAX72XX]
*/

#include <SPI.h>
#include <MD_MAX72xx.h>
#include <esp8266.h>

SPIClass SPI(1);
ESP8266Class mySerial(1);

// Define the number of devices we have in the chain and the hardware interface
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4 
#define CS_PIN    10  // connect CS pin to GPIO-10
/* NOTE: You can use any GPIO Pin from the board */

MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

char stringBuffer[1000] = {0, };
int count;

#define  DELAYTIME  100  // in milliseconds

void scrollText(const char *p)
{
  uint8_t charWidth;
  uint8_t cBuf[8];  // this should be ok for all built-in fonts

  mx.clear();

  while (*p != '\0')
  {
    charWidth = mx.getChar(*p++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);

    for (uint8_t i=0; i<=charWidth; i++)  // allow space between characters
    {
      mx.transform(MD_MAX72XX::TSL);
      if (i < charWidth)
        mx.setColumn(0, cBuf[i]);
      delay(DELAYTIME);
    }
  }
}

void setup() {
  // Open serial communications and wait for port to open:
  delay(1000);
  Serial.begin(115200);
  mySerial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  mx.begin();
}

void loop() {
  count = 0;
  for(;count<1000;count++){
    stringBuffer[count] = 0;
  }
  Serial.print("Ready to receive text....");
  while(1){
    count = 0;
    while(mySerial.available() > 0){
      stringBuffer[count++] = mySerial.read();
    }
    if(stringBuffer[count-1] == '\n'){
      stringBuffer[count-1] = '\0';
      break;
    }
  }
  Serial.println("received.");
  count = 0;
  for(;count<1000;count++){
    Serial.print(stringBuffer[count]);
  }
  Serial.println();

  while(1){
    scrollText(stringBuffer);
    if(mySerial.available() > 0){
      uint8_t newText = mySerial.read();
      if(newText == 'N')break;
    }
  }
}
