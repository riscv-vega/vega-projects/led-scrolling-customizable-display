/* 
    Display text on 8×8 LED Matrix from text file in SD Card Example 02
    by VEGA Processors, www.vegaprocessors.in

    Library Required: MD_MAX72xx [https://github.com/MajicDesigns/MD_MAX72XX]
*/


#include <SPI.h>
#include <SD.h>
#include <MD_MAX72xx.h>

SPIClass SPI(0);
SPIClass SPI_1(1);

// Define the number of devices we have in the chain and the hardware interface
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4
#define CS_PIN_1    10  // connect CS pin of MAX7219 LED Matrix to GPIO-10
#define CS_PIN_2    9   // connect CS pin of SD-CARD Module to GPIO-9

File myFile;
MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, SPI_1, CS_PIN_1, MAX_DEVICES);

char Buffer[1000];
int count;

#define  DELAYTIME  100  // in milliseconds

void setup() {
  // Open serial communications and wait for port to open:
  delay(1000);
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.print("Initializing SD card...");

  if (!SD.begin(CS_PIN_2)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  
  // Initializing LED Matrix
  mx.begin();
//  ** Use below function to remove existing test.txt file from your microSD card **  //
//  removeFile(); 

// ** Use below function to create test.txt file in your microSD card ** //
//  createFile();
}

void loop() {
  count = 0;
  for(;count<1000;count++){
    Buffer[count] = 0;
  }

  if (SD.exists("test.txt")) {
    Serial.println("test.txt exists.");
  } else {
    Serial.println("test.txt doesn't exist.");
    while(1){
      scrollText("test.txt doesn't exist.");
    }
  }
  
  // re-open the file for reading:
  myFile = SD.open("test.txt"); // If you have existing txt file with different name, then change the file name here.
  if (myFile) {
    Serial.println("test.txt:");

    // read from the file until there's nothing else in it:
    count = 0;
    for(;myFile.available();count++){
      Buffer[count] = myFile.read();
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  count = 0;
  for(;count<1000;count++){
    Serial.print(Buffer[count]);
  }
  Serial.println();
  
  while(1){
    scrollText(Buffer);
  }
}

void createFile(void) {  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
    myFile.print("VEGA PROCESSORS"); // ********************************* Modify the text message here **************************** //
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}

void removeFile(void) {
  // use this function to remove text.txt to avoid appending
  if (SD.exists("test.txt")) {
    Serial.print("Removing test.txt...");
    SD.remove("test.txt");
    Serial.println("done.");
  } else {
    Serial.println("test.txt doesn't exist.");
  }
}

void scrollText(const char *p) {
  uint8_t charWidth;
  uint8_t cBuf[8];  // this should be ok for all built-in fonts

  mx.clear();
  while (*p != '\0') {
    charWidth = mx.getChar(*p++, sizeof(cBuf) / sizeof(cBuf[0]), cBuf);

    for (uint8_t i=0; i<=charWidth; i++)  // allow space between characters
    {
      mx.transform(MD_MAX72XX::TSL);
      if (i < charWidth)
        mx.setColumn(0, cBuf[i]);
      delay(DELAYTIME);
    }
  }
}
